const Koa = require('koa');
const Router = require('koa-router');
const Logger = require('koa-logger');
const BodyParser = require('koa-bodyparser');

const app = new Koa();
const router = new Router();

// Routes
const menuRoutes = require('./routes/menu');
const tableRoutes = require('./routes/table');
router.use('/menu', menuRoutes);
router.use('/table', tableRoutes);

router.get('/', async (ctx) => {
  ctx.body = 'Hello';
});

app.use(Logger());
app.use(BodyParser());
app.use(router.routes([menuRoutes])).use(router.allowedMethods());

app.listen(3000, () => {
  console.log('Server is running on port', 3000);
});
