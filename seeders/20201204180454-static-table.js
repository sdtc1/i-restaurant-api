'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('Tables', feedTables(), {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};

function feedTables() {
  let tables = [];
  for (let i = 1; i <= 10; i++) {
    tables.push({
      name: `${i}`,
      description: `Table #${i}`,
      notes: '',
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  return tables;
}
