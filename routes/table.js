const Router = require('koa-router');
const router = new Router();

// Models
const db = require('../models');
const Table = db.Table;
const Bill = db.Bill;

router.get('/', async (ctx) => {
  const tables = await Table.findAll();
  ctx.body = tables;
});

router.patch('/open/:id', async (ctx) => {
  const { id } = ctx.params;

  await Table.update({ isAvailable: true }, { where: { id } });

  const updatedTable = await Table.findOne({ where: { id } });

  // Create bill then link it to with tableID
  const createdBill = await Bill.create({
    tableId: id,
    totalPrice: 0,
    isPaid: false,
  });

  ctx.body = {
    openedTable: updatedTable,
    bill: createdBill,
  };
});

module.exports = router.routes();
