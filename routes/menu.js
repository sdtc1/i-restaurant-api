const Router = require('koa-router');
const router = new Router();

// Models
const db = require('../models');
const Menu = db.Menu;

router.get('/', async (ctx) => {
  const menus = await Menu.findAll();
  ctx.body = menus;
});

router.post('/', async (ctx) => {
  const createdMenu = await Menu.create(ctx.request.body);
  ctx.body = createdMenu;
});

router.patch('/:id', async (ctx) => {
  await Menu.update(ctx.request.body, { where: { id: ctx.params.id } });
  ctx.body = 'Menu was updated';
});

router.delete('/:id', async (ctx) => {
  const menus = await Menu.findAll({
    where: {
      id: ctx.params.id,
    },
  });

  if (menus.length === 0) {
    ctx.status = 404;
    ctx.body = 'Your menu id not found';
  } else {
    const deletedMenu = await Menu.destroy({ where: { id: ctx.params.id } });
    console.log('deleted menu', deletedMenu);
    ctx.body = `Menu: ${menus[0].name} was deleted`;
  }
});

module.exports = router.routes();
